from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(query):
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    try:
        return api_dict['photos'][0]['src']['original']
    except (KeyError, IndexError):
        return{"picture_url":None}

def get_weather_info(city, state):
    location_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(location_url)
    api_dict = json.loads(response.content)

    try:
        latitude = api_dict[0]['lat']
        longitude = api_dict[0]['lon']

    except (KeyError, IndexError):
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_dict = json.loads(weather_response.content)

    try:
        return {
            "description": weather_dict["weather"][0]["description"],
            "temp": weather_dict["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
